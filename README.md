# Life

Simple browser game which emulate some kind of the life.

# Used technology
* Vanilla HTML
* Vanilla CSS
* Vanilla JS
* Node (npm)
* Webpack

# Startup:
1) Install all dependencies by console command: `npm install` (Required NodeJS)

2) Build all stuff by console command (Files will in "dist" directory):  

* Windows:`npm run build`
* Linux: `node ./node_modules/.bin/webpack`

3) Open file "dist/.index.html" with your browser (Chrome recommended).
