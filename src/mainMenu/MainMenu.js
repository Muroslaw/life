import "./Menu.css";
import {eventSystem} from "../utils/EventSystem";

/**
 * Class (controller) for Main menu page.
 */
export class MainMenu {
    constructor() {
        this.view = document.createElement("section");
        this.view.className = "main-menu__container";

        this.createLogo();
        this.createItem("New game", () => eventSystem.dispatchEvent("Show new game window"));
        this.createItem("Options", () => eventSystem.dispatchEvent("Show options window"));
    }

    getView() {
        return this.view;
    }

    createLogo() {
        const menuLogo = document.createElement("div");
        menuLogo.className = "main-menu__logo";
        menuLogo.innerText = "Life";
        this.view.appendChild(menuLogo);
    }

    createItem(name, action) {
        const item = document.createElement("div");
        item.className = "main-menu__item";
        item.innerText = name;
        item.onclick = action;
        this.view.appendChild(item);
    }
}