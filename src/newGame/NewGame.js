import "./NewGame.css";
import {eventSystem} from "../utils/EventSystem";
import {options} from "../utils/Options";

/**
 * Class (controller) for New Game page.
 */
export class NewGame {
    constructor() {
        this.view = document.createElement("section");
        this.view.className = "new-game_container";

        this.createHeader();
        this.createContent();
        this.fieldWidth = options.getOption("Default game field width");
        this.fieldHeight = options.getOption("Default game field height");
    }

    getView() {
        return this.view;
    }

    createHeader() {
        const header = document.createElement("header");
        header.className = "new-game__header";

        const back = document.createElement("div");
        back.className = "new-game__back";
        back.innerText = "Back";
        back.onclick = () => eventSystem.dispatchEvent("Show main menu");
        header.appendChild(back);

        const title = document.createElement("div");
        title.className = "new-game__title";
        title.innerText = "New game";
        header.appendChild(title);

        const start = document.createElement("div");
        start.className = "new-game__start";
        start.innerText = "Create game field";
        start.onclick = () => eventSystem.dispatchEvent("Show game field", {
            width: this.fieldWidth,
            height: this.fieldHeight
        });
        header.appendChild(start);

        this.view.appendChild(header);
    }

    createContent() {
        const content = document.createElement("main");
        content.className = "new-game__content";

        content.appendChild(this.createWidthInput());
        content.appendChild(this.createHeightInput());

        this.view.appendChild(content);
    }

    createWidthInput() {
        const label = document.createElement("label");
        label.className = "new-game__input-label";
        label.innerText = "Field width";

        const input = document.createElement("input");
        input.type = "number";
        input.value = options.getOption("Default game field width");
        input.min = 1;
        input.oninput = () => {
            this.fieldWidth = input.value;
        };

        label.appendChild(input);
        return label;
    }

    createHeightInput() {
        const label = document.createElement("label");
        label.className = "new-game__input-label";
        label.innerText = "Field height";

        const input = document.createElement("input");
        input.type = "number";
        input.value = options.getOption("Default game field height");
        input.min = 1;
        input.oninput = () => {
            this.fieldHeight = input.value;
        };

        label.appendChild(input);
        return label;
    }
}