import "./root.css";
import {eventSystem} from "../utils/EventSystem";
import {MainMenu} from "../mainMenu/MainMenu";
import {NewGame} from "../newGame/NewGame";
import {OptionsUI} from "../optionsUI/OptionsUI";
import {Game} from "../game/Game";

/**
 * Class which change view of the page.
 */
export class Root {
    constructor() {
        this.view = document.createElement("section");
        this.view.className = "game-root";
        // Initial view
        this.showMainMenu();

        eventSystem.createListener("Show main menu", () => this.showMainMenu());
        eventSystem.createListener("Show new game window", () => this.showNewGameWindow());
        eventSystem.createListener("Show options window", () => this.showOptionsWindow());
        eventSystem.createListener("Show game field", ({width, height}) => this.showGameField(width, height));
    }

    showMainMenu() {
        this.view.innerText = "";
        this.view.appendChild(new MainMenu().getView());
    }

    showNewGameWindow() {
        this.view.innerText = "";
        this.view.appendChild(new NewGame().getView());
    }

    showOptionsWindow() {
        this.view.innerText = "";
        this.view.appendChild(new OptionsUI().getView());
    }

    showGameField(width, height) {
        this.view.innerText = "";
        this.view.appendChild(new Game(width, height).getView());
    }
}
