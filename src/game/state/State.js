import {eventSystem} from "../../utils/EventSystem";

/**
 * Place where game state saving and sending state to updating view.
 */
export class State {
    constructor() {
        this.state = {}
    }

    /**
     * Return current state.
     * @returns {Object}
     */
    getState() {
        return this.state;
    }

    /**
     * Replace state with provided.
     * @param {Object} newState
     */
    setState(newState) {
        this.state = newState;
        eventSystem.dispatchEvent("Game state updated", this.getState())
    }
}

