import {State} from "./state/State";
import {Controller} from "./controller/Controller";
import {View} from "./view/View";

/**
 * Main part of the app - game LIFE itself.
 */
export class Game {
    /**
     * Create view, init game state and game logic (Controller).
     * @param {number} width - Game field width.
     * @param {number} height - Game field height.
     */
    constructor(width, height) {
        this.view = new View(width, height).view;
        this.state = new State();
        new Controller(this.state, width, height);
    }

    getView() {
        return this.view;
    }
}
