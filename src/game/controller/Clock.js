import {eventSystem} from "../../utils/EventSystem";

/**
 * Class which do tick and push game engine to to update.
 */
export class Clock {
    /**
     * @param {number} tickSize - Size of the 1 tick in milliseconds.
     */
    constructor(tickSize) {
        this.tickSize = tickSize;
        this.clockLoop = null;

        eventSystem.createListener("Start clock", () => this.start(this.tickSize));
        eventSystem.createListener("Stop clock", () => this.stop());
    }

    /**
     * Start clock. On every tick dispatching "New tick" event.
     * @param {number} tickSize - Size of the 1 tick in milliseconds.
     */
    start(tickSize) {
        if (!this.clockLoop) {
            this.clockLoop = setInterval(() => eventSystem.dispatchEvent("New tick"), tickSize)
        }
    }

    stop() {
        if (this.clockLoop) {
            clearInterval(this.clockLoop);
            delete this.clockLoop;
        }
    }
}
