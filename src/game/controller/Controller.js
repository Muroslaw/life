import {Clock} from "./Clock";
import {options} from "../../utils/Options";
import {eventSystem} from "../../utils/EventSystem";

/**
 * Controller (engine) for the game.
 */
export class Controller {
    /**
     * @param {State} gameState
     * @param {number} width - Game field width.
     * @param {number} height - Game field height.
     */
    constructor(gameState, width, height) {
        this.state = {};
        this.updateState = () => gameState.setState(this.state);

        this.state.field = this.createEmptyField(width, height);
        this.state.gameInProgress = false;
        this.state.gameFieldChanged = false;

        // Init clock.
        const tickSize = options.getOption("Tick size");
        this.clock = new Clock(tickSize);

        eventSystem.createListener("Start game", () => this.startGame());
        eventSystem.createListener("Stop game", () => this.stopGame());
        eventSystem.createListener("Item click", ({rowIndex, columnIndex}) => this.itemClickHandler(rowIndex, columnIndex));
        eventSystem.createListener("Randomize field", () => this.randomizeField());
        // Init state of view.
        this.updateState();
    }

    /**
     * Start clock. Update state on every new tick.
     */
    startGame() {
        if (this.state.gameInProgress) {
            return;
        }
        this.state.gameInProgress = true;

        eventSystem.createListener("New tick", () => {
            this.state.gameFieldChanged = false;
            this.forEachItem((rowIndex, columnIndex) => this.chooseDestinyForTheItem(rowIndex, columnIndex));

            if (!this.state.gameFieldChanged) {
                this.stopGame();
            }

            this.updateState();
        });

        eventSystem.dispatchEvent("Start clock");
    }

    /**
     * Stop clock. Update state.
     */
    stopGame() {
        this.state.gameInProgress = false;
        this.state.gameFieldChanged = false;
        eventSystem.dispatchEvent("Stop clock");
        this.updateState();
    }

    /**
     * By provided coordinates get item, and calculate it destiny(Be marked or unmarked).
     * @param {number} rowIndex
     * @param {number} columnIndex
     */
    chooseDestinyForTheItem(rowIndex, columnIndex) {
        const countOfMarkedNeighbors = this.getCountOfMarkedNeighbors(rowIndex, columnIndex);
        const cellMarked = this.state.field[rowIndex][columnIndex] === true;

        // Mark item
        if (!cellMarked && countOfMarkedNeighbors === 3) {
            this.state.gameFieldChanged = true;
            this.state.field[rowIndex][columnIndex] = true;
            return;
        }
        // Un-mark item
        if (cellMarked && !(countOfMarkedNeighbors === 2 || countOfMarkedNeighbors === 3)) {
            this.state.gameFieldChanged = true;
            this.state.field[rowIndex][columnIndex] = false;
        }
    }
    /**
     * Calculate count of marked item neighbors.
     * @param {number} rowIndex
     * @param {number} columnIndex
     * @returns {number}
     */
    getCountOfMarkedNeighbors(rowIndex, columnIndex) {
        let count = 0;
        this.forEachItemsNeighbors(rowIndex, columnIndex, (neighborRow, neighborColumn) => {
            if (this.state.field[neighborRow][neighborColumn] === true) {
                count++;
            }
        });
        return count;
    }

    /**
     * Handler of click on the item
     * @param {number} rowIndex - Item's row
     * @param {number} columnIndex - Item's column
     */
    itemClickHandler(rowIndex, columnIndex) {
        if (!this.state.gameInProgress) {
            this.state.field[rowIndex][columnIndex] = !this.state.field[rowIndex][columnIndex];
            this.updateState();
        }
    }

    /**
     * Create empty game field matrix.
     * false -> unmarked.
     * true -> marked.
     *
     * @param {number} width
     * @param {number} height
     * @returns {Array}
     */
    createEmptyField(width, height) {
        const field = [];
        for (let i = 0; i < height; i++) {
            const rowIndex = [];
            rowIndex.length = width;
            rowIndex.fill(false);
            field[i] = rowIndex;
        }
        return field;
    }

    /**
     * Fill game field with by random.
     */
    randomizeField() {
        this.forEachItem((rowIndex, columnIndex) => {
            this.state.field[rowIndex][columnIndex] = Math.random() > 0.5;
        });
        this.updateState();
    }


    /**
     * For each item of game field do callback
     * @param {Function} callback
     */
    forEachItem(callback) {
        // The "for" loop used because it's faster that "Array.forEach".
        for (let i = 0; i < this.state.field.length; i++) {
            for (let j = 0; j < this.state.field[i].length; j++) {
                callback(i, j);
            }
        }
    }

    /**
     * For each neighbor of item from the game field do callback.
     *
     * @param {number} rowIndex
     * @param {number} columnIndex
     * @param {Function} callback
     */
    forEachItemsNeighbors(rowIndex, columnIndex, callback) {
        // Array with value nas switch-case used for more readability.
        ["top", "top-right", "right", "bottom-right", "bottom", "bottom-left", "left", "top-left",].forEach((side) => {
            switch (side) {
                case "top": {
                    let neighborRow = rowIndex - 1;
                    if (neighborRow < 0) {
                        neighborRow = this.state.field.length - 1;
                    }
                    return callback(neighborRow, columnIndex);
                }

                case "top-right": {
                    let neighborRow = rowIndex - 1;
                    if (neighborRow < 0) {
                        neighborRow = this.state.field.length - 1;
                    }
                    let neighborColumn = columnIndex + 1;
                    if (neighborColumn > this.state.field[rowIndex].length - 1) {
                        neighborColumn = 0;
                    }
                    return callback(neighborRow, neighborColumn);
                }

                case "right": {
                    let neighborColumn = columnIndex + 1;
                    if (neighborColumn > this.state.field[rowIndex].length - 1) {
                        neighborColumn = 0;
                    }
                    return callback(rowIndex, neighborColumn)
                }

                case "bottom-right": {
                    let neighborRow = rowIndex + 1;
                    if (neighborRow > this.state.field.length - 1) {
                        neighborRow = 0;
                    }
                    let neighborColumn = columnIndex + 1;
                    if (neighborColumn > this.state.field[rowIndex].length - 1) {
                        neighborColumn = 0;
                    }
                    return callback(neighborRow, neighborColumn);
                }

                case "bottom": {
                    let neighborRow = rowIndex + 1;
                    if (neighborRow > this.state.field.length - 1) {
                        neighborRow = 0;
                    }
                    return callback(neighborRow, columnIndex);
                }

                case "bottom-left": {
                    let neighborRow = rowIndex + 1;
                    if (neighborRow > this.state.field.length - 1) {
                        neighborRow = 0;
                    }
                    let neighborColumn = columnIndex - 1;
                    if (neighborColumn < 0) {
                        neighborColumn = this.state.field[rowIndex].length - 1;
                    }
                    return callback(neighborRow, neighborColumn);
                }

                case "left": {
                    let neighborColumn = columnIndex - 1;
                    if (neighborColumn < 0) {
                        neighborColumn = this.state.field[rowIndex].length - 1;
                    }
                    return callback(rowIndex, neighborColumn);
                }

                case "top-left": {
                    let neighborRow = rowIndex - 1;
                    if (neighborRow < 0) {
                        neighborRow = this.state.field.length - 1;
                    }
                    let neighborColumn = columnIndex - 1;
                    if (neighborColumn < 0) {
                        neighborColumn = this.state.field[rowIndex].length - 1;
                    }
                    return callback(neighborRow, neighborColumn);
                }
            }
        });
    }
}
