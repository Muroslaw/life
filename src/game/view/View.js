import "./View.css"
import {eventSystem} from "../../utils/EventSystem";
import {options} from "../../utils/Options";

/**
 * Class which render game to the page.
 */
export class View {
    /**
     * @param {number} width - Game field width.
     * @param {number} height - Game field height.
     */
    constructor(width, height) {
        this.useSVG = options.getOption("Use SVG");
        this.width = width;
        this.height = height;
        this.itemsGrid = []; // Matrix which contain game field elements.

        this.view = document.createElement("section");
        this.view.className = "game_container";
        this.view.appendChild(this.createHeader());
        this.view.appendChild(this.createContent());

        // In that place we catching the "Game state update".
        // We updating view! Not removing and creating new because it's to long.
        eventSystem.createListener("Game state updated", (state) => {
            this.field = state.field;
            this.gameInProgress = state.gameInProgress;
            this.updateHeader();
            this.updateContent();
        });
    }

    createHeader() {
        const header = document.createElement("header");
        header.className = "game__header";

        const back = document.createElement("div");
        back.className = "game__back";
        back.innerText = "Back";
        back.onclick = () => {
            eventSystem.dispatchEvent("Stop game");
            eventSystem.dispatchEvent("Show new game window");
        };
        header.appendChild(back);

        const title = document.createElement("div");
        title.className = "game__title";
        title.innerText = "Life";
        header.appendChild(title);

        const controls = document.createElement("div");
        controls.className = "game__controls";

        const button = document.createElement("div");
        button.className = "game__button";
        button.innerText = "Start";
        button.onclick = () => eventSystem.dispatchEvent("Start game");
        controls.appendChild(button);

        const randomize = document.createElement("div");
        randomize.className = "game__randomize";
        randomize.innerText = "Randomize field";
        randomize.onclick = () => eventSystem.dispatchEvent("Randomize field");
        controls.appendChild(randomize);
        header.appendChild(controls);

        return header;
    }

    updateHeader() {
        const button = document.querySelector(".game__button");
        if (button) {
            if (this.gameInProgress) {
                button.innerText = "Stop";
                button.onclick = () => eventSystem.dispatchEvent("Stop game");
            } else {
                button.innerText = "Start";
                button.onclick = () => eventSystem.dispatchEvent("Start game");
            }
        }

        const randomize = document.querySelector(".game__randomize");
        if (randomize) {
            if (this.gameInProgress) {
                randomize.style.display = "none";
            } else {
                randomize.style.display = "block";
            }
        }
    }

    createContent() {
        const content = document.createElement("main");
        content.className = "game-field";
        if (this.useSVG) {
            content.appendChild(this.createSVGGrid());
        } else {
            content.appendChild(this.createTable());
        }
        return content;
    }

    updateContent() {
        for (let rowIndex = 0; rowIndex < this.field.length; rowIndex++) {
            for (let columnIndex = 0; columnIndex < this.field[rowIndex].length; columnIndex++) {
                const itemSelected = this.field[rowIndex][columnIndex];
                // Using itemsGrid matrix because it's faster than using document.querySelector
                if (itemSelected) {
                    this.itemsGrid[rowIndex][columnIndex].classList.add("selected");
                } else {
                    this.itemsGrid[rowIndex][columnIndex].classList.remove("selected");
                }
            }
        }
    }

    createTable() {
        const table = document.createElement("div");
        table.className = "game-table";

        for (let rowIndex = 0; rowIndex < this.height; rowIndex++) {
            const row = document.createElement("div");
            row.className = "game-table__row";
            this.itemsGrid[rowIndex] = [];

            for (let columnIndex = 0; columnIndex < this.width; columnIndex++) {
                const item = document.createElement("div");
                item.className = "game-table__item";
                item.onclick = () => eventSystem.dispatchEvent("Item click", {rowIndex, columnIndex});
                this.itemsGrid[rowIndex].push(item);
                row.appendChild(item);
            }
            table.appendChild(row);
        }

        return table;
    }

    createSVGGrid() {
        const itemWidth = 10;
        const itemHeight = 10;

        const svgNS = "http://www.w3.org/2000/svg";
        const svg = document.createElementNS(svgNS, "svg");
        svg.setAttribute("class", "svg-canvas");
        // Calculate canvas sizes.
        svg.setAttribute("width", `${itemWidth * this.width}`);
        svg.setAttribute("height", `${itemHeight * this.height}`);

        // Canvas coordinates for inserting elements.
        let x = 0;
        let y = 0;

        for (let rowIndex = 0; rowIndex < this.height; rowIndex++) {
            this.itemsGrid[rowIndex] = [];
            for (let columnIndex = 0; columnIndex < this.width; columnIndex++) {
                const item = document.createElementNS(svgNS, "rect");
                item.setAttribute("class", "svg-canvas__item");
                item.setAttribute("width", `${itemWidth}`);
                item.setAttribute("height", `${itemHeight}`);
                item.setAttribute("x", `${x}`);
                item.setAttribute("y", `${y}`);
                item.onclick = () => eventSystem.dispatchEvent("Item click", {rowIndex, columnIndex});
                this.itemsGrid[rowIndex].push(item);
                svg.appendChild(item);
                x += itemWidth;
            }
            y += itemHeight;
            x = 0;
        }

        return svg;
    }
}