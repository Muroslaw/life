import "./OptionsUI.css"
import {eventSystem} from "../utils/EventSystem";
import {options} from "../utils/Options";

/**
 * Class (controller) for options UI.
 */
export class OptionsUI {
    constructor() {
        this.view = document.createElement("section");
        this.view.className = "options_container";
        this.options = options.options;

        this.createHeader();
        this.createContent();
    }

    getView(){
        return this.view;
    }

    createHeader() {
        const header = document.createElement("header");
        header.className = "options__header";

        const back = document.createElement("div");
        back.className = "options__back";
        back.innerText = "Back";
        back.onclick = () => eventSystem.dispatchEvent("Show main menu");
        header.appendChild(back);

        const title = document.createElement("div");
        title.className = "options__title";
        title.innerText = "Options";
        header.appendChild(title);

        const save = document.createElement("div");
        save.className = "options__save";
        save.innerText = "Save";
        save.onclick = () => {
            options.overwriteOptions(this.options);
            eventSystem.dispatchEvent("Show main menu");
        };
        header.appendChild(save);

        this.view.appendChild(header);
    }

    createContent() {
        const content = document.createElement("main");
        content.className = "options__content";

        Object.entries(this.options).forEach(([key, value]) => {
            content.appendChild(this.createOptionInput(key, value));
        });

        this.view.appendChild(content);
    }

    /**
     *  Create input for option.
     *  For different types of value will be created different types of inputs.
     * @param {string} key - Label of input.
     * @param {string | number | string} value - Current option value.
     * @returns {HTMLElement}
     */
    createOptionInput(key, value) {
        const label = document.createElement("label");
        label.className = "options__input-label";
        label.innerText = key;

        const input = document.createElement("input");

        if (typeof value === "boolean") {
            input.type = "checkbox";
            input.checked = value;
            input.onclick = () => {
                this.options[key] = !this.options[key];
            }
        } else {
            if (typeof value === "number") {
                input.min = 1;
                input.type = "number";
            } else {
                input.type = "text"
            }

            input.value = value;
            input.oninput = () => {
                this.options[key] = input.value;
            };
        }
        label.appendChild(input);

        return label;
    }
}