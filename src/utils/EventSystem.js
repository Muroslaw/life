/**
 * Custom global event system.
 */
class EventSystem {
    constructor() {
        this.events = {};
    }

    /**
     * Create listener for event.
     * @param {string} eventName
     * @param {Function} callback
     */
    createListener(eventName, callback) {
        this.events[eventName] = callback;
    }

    /**
     * Dispatch event. Run callback attached to listener.
     * @param {string } eventName
     * @param {*} data - Data for listener callback.
     */
    dispatchEvent(eventName, data) {
        if (this.events[eventName]) {
            this.events[eventName](data);
        }
    }
}

// Create single instance for whole app.
export const eventSystem = new EventSystem();
