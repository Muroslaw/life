/**
 * Global options of the app.
 */
class Options {
    constructor() {
        this.options = {};
        this.setDefaultOptions();
    }

    /**
     * Get option by key.
     * @param {string} key
     * @returns {*}
     */
    getOption(key) {
        return this.options[key];
    }

    /**
     * Overwrite options by provided object.
     * @param {object} newOptions
     */
    overwriteOptions(newOptions) {
        this.options = newOptions;
    }

    /**
     * Init default options.
     */
    setDefaultOptions() {
        this.overwriteOptions({
            "Tick size": 500,
            "Default game field width": 100,
            "Default game field height": 100,
            "Use SVG": false
        });
    }
}

// Create single instance for the whole app.
export const options = new Options();
